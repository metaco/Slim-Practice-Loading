<?php

namespace App\Providers;


use EasyWeChat\Foundation\Application;

class ServiceProvider
{
    protected $provider = [
        'WeChat' => Application::class,
        //'WeChat' => 'Application'
    ];

    protected $instances;

    
    public function __construct($config)
    {
        $this->registerServices($config);
    }

    /**
     * @param $config
     */
    protected function registerServices($config)
    {
        foreach ($this->provider as $key => $provider){
            $this->register($key, new $provider($config));
        }
    }

    /**
     * @return mixed
     */
    public function getInstance()
    {
        return $this->instances;
    }

    /**
     * @param $key
     * @param $provider
     */
    protected function register($key, $provider)
    {
        if(class_exists($provider)){
            $this->instances[$key] = $provider;
        }
    }
}