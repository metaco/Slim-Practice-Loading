<?php

namespace App\Controllers;

use App\Controllers\Controller;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

//use App\Providers\ServiceProvider;

use EasyWeChat\Foundation\Application;

/**
 * Class HomeController
 * @package App\Controllers
 */
class HomeController extends Controller
{
    public function users($request, $response)
    {
        $username = 'elliot';
        return $this->view->render($response,'home.twig',
            ['username' => $username]);
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     */
    public function locate(RequestInterface $request, ResponseInterface $response)
    {
        echo $request->getUri();
        echo $response->getStatusCode();
        echo $response->getBody();
        echo $request->getRequestTarget();
        echo $request->getProtocolVersion();
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function login(RequestInterface $request, ResponseInterface $response)
    {
        $method = ['GET'];
        if(in_array($request->getMethod(), $method)){
            return $this->view->render($response, 'login.twig');
        }else{
            $data = $request->getParams();
            $rs = $request->getBody();
            $d = $rs->getContents();
            var_dump($rs);
        }
        //var_dump(get_loaded_extensions());

        //return $this->view->render($response, 'login.twig');
    }


    public function formData(RequestInterface $request, ResponseInterface $response)
    {
        $data = $request->getParams();

        $rs = $request->getBody();

        $d = $rs->getContents();
        var_dump($d);
    }


    public function fetchData()
    {
        $rest = $this->db->get('ims_mc_members',
            ['realname','idcard'], ['mobile'=>'13609131798']);

        var_dump($rest);
    }




    public function subscribe()
    {
        //$userService = $this->WeChat->user;

        //$datas = $userService->lists()->get('data');

        //$openid = 'oY_1bxAwH5ijKyW3diUepSsGMbwQ';
        //$datas = $userService->get($openid);
        //$datas = $datas->nickname;
        //var_dump($datas);
    }


    /*public function loader()
    {
        $settings = $this->container->get('settings');
        $loader = new ServiceProvider($settings['WeChat']);

        $wechat = $loader->getInstance();

        var_dump($wechats);
    }*/


    public function openIds()
    {
        $settings = $this->container->get('settings');

        // 实例化微信开发类
        $WeChat = new Application($settings['WeChat']);

        // 获取用户服务组件
        $userService = $WeChat->user;
        // 获取当前所有关注过公众号的 openid
        $openidData = $userService->lists();

        var_dump($openidData);
    }

    public function notice()
    {
        $settings = $this->container->get('settings');

        // 实例化微信开发类
        $WeChat = new Application($settings['WeChat']);
        // 获取信息服务组件
        $noticeService = $WeChat->notice;


        $res = $noticeService->send([
            'touser'      => 'oY_1bxAwH5ijKyW3diUepSsGMbwQ',
            'template_id' => 'ofyHow9wnpwXzgid95qgsHNSwAgwI76n1OB9SbCQcmI',
            //'url' => '',
            'topcolor' => '#f7f7f7',
            'data' => [
                "first"    => "您的赛格国际会员卡已激活 \n在-卡包-里即可查看",
                "keyword1" => ['03943545',"#69008C"],
                "keyword2" => [1000,"#69008C"],
                "remark"   => "感谢您加入会员，我们将竭诚为您服务。",
            ],
        ]);
        var_dump($res);
    }

}